// import React from 'react';
// import {useFormik} from 'formik';
// import {OutlinedInput, InputAdornment, InputLabel} from "@mui/material";
// import EmailIcon from '@mui/icons-material/Email';
// import LockIcon from '@mui/icons-material/Lock';
// import Button from '@mui/material/Button';
// import * as Yup from 'yup';
//
// import './loginationForm.scss';
//
// const SignupForm = () => {
//
//     const validationSchema = Yup.object({
//         email: Yup.string().email('Invalid email address').required('Required'),
//     })
//
//     const formik = useFormik({
//         initialValues: {
//             email: '',
//             password: ''
//         },
//         onSubmit: (values) => {
//             console.log(JSON.stringify(values));
//         },
//         validationSchema
//     })
//
//
//     return (
//         <div className="form">
//             <div className="text-module">
//                 <h4>Welcome!</h4>
//                 <h2>Join The Community</h2>
//             </div>
//             <form className="input-module" onSubmit={formik.handleSubmit}>
//
//                 <InputLabel className="label" shrink htmlFor="email">
//                     E-Mail or Username
//                 </InputLabel>
//                 <OutlinedInput
//                     className="input_mar"
//                     fullWidth
//                     id="email"
//                     name="email"
//                     type="email"
//                     placeholder="e.g.: elonmusk@mars.com"
//                     value={formik.values.email}
//                     onChange={formik.handleChange}
//                     startAdornment={
//                         <InputAdornment position="start">
//                             <EmailIcon/>
//                         </InputAdornment>
//                     }
//                 />
//
//                 <InputLabel className="label" shrink htmlFor="password">
//                     Password
//                 </InputLabel>
//                 <OutlinedInput
//                     className="input"
//                     fullWidth
//                     id="password"
//                     name="password"
//                     type="password"
//                     placeholder="e.g.: X Æ A-12"
//                     value={formik.values.password}
//                     onChange={formik.handleChange}
//                     startAdornment={
//                         <InputAdornment position="start">
//                             <LockIcon/>
//                         </InputAdornment>
//                     }
//                 />
//
//                 <div className="button-module">
//                     <Button
//                         fullWidth
//                         variant="contained"
//                         type="submit">
//                         Sign Up
//                     </Button>
//                     <div className="button-module_title">
//                         <p>Already a member?</p>
//                         <a href="/#">Login here →</a>
//                     </div>
//                 </div>
//             </form>
//         </div>
//     );
// };
// export default SignupForm;

import React from 'react';
import {Formik} from 'formik';
import {OutlinedInput, InputAdornment, InputLabel} from "@mui/material";
import EmailIcon from '@mui/icons-material/Email';
import LockIcon from '@mui/icons-material/Lock';
import Button from '@mui/material/Button';
import * as Yup from 'yup';

import './loginationForm.scss';

const SignupForm = () => {

    return (
        <div className="form">

            <div className="text-module">
                <h4>Welcome!</h4>
                <h2>Join The Community</h2>
            </div>
            <Formik
                initialValues={{email: '', password: ''}}
                onSubmit={(values) => {
                    console.log(JSON.stringify(values));
                }}
                validationSchema={Yup.object({
                    email: Yup.string()
                        .email()
                })}
            >
                {(props) => {
                    const {
                        values,
                        touched,
                        errors,
                        handleChange,
                        handleBlur,
                        handleSubmit
                    } = props;
                    return (<form className="input-module" onSubmit={handleSubmit}>
                            <InputLabel className="label" shrink htmlFor="email">
                                E-Mail or Username
                            </InputLabel>
                            <OutlinedInput
                                className="input_mar"
                                fullWidth
                                id="email"
                                name="email"
                                type="email"
                                placeholder="e.g.: elonmusk@mars.com"
                                value={values.email}
                                onChange={handleChange}
                                error={errors.email && touched.email}
                                onBlur={handleBlur}
                                color="success"
                                startAdornment={
                                    <InputAdornment position="start">
                                        <EmailIcon/>
                                    </InputAdornment>
                                }
                            />

                            <InputLabel className="label" shrink htmlFor="password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                className="input"
                                fullWidth
                                id="password"
                                name="password"
                                type="password"
                                placeholder="e.g.: X Æ A-12"
                                value={values.password}
                                onChange={handleChange}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <LockIcon/>
                                    </InputAdornment>
                                }
                            />

                            <div className="button-module">
                                <Button
                                    fullWidth
                                    variant="contained"
                                    type="submit">
                                    Sign Up
                                </Button>
                                <div className="button-module_title">
                                    <p>Already a member?</p>
                                    <a href="/#">Login here →</a>
                                </div>
                            </div>
                        </form>
                    );
                }}
            </Formik>
        </div>
    );
};
export default SignupForm;